

// WAP to count odd digit of given number.



class Count {

	public static void main(String[] args) {

		int num = 942111423;

		int count = 0;

		while(num != 0) {

			int i = num % 10;

			if(i % 2 == 1) {

				count++;
			}
			num = num / 10;
		}
		System.out.println(count);
	}
}
