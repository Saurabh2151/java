

class Core2Web {

        public static void main(String[] args) {

                String var1 = null;
                var1 = args[0] + args[1] + args[2];
                System.out.println(var1);
        }
}

// OP --> Exception : ArrayIndexOutOfBoundsException: Index 0 out of bounds for length 0

/* Explanation : Arguments stored in String[] args are in string formate 
                 we access them and use '+'. + in string is used to concat
		 So all the values in array args are simply concatenated.

*/

