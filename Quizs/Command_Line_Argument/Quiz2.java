

class Core2Web{
	public static  void main(String[] args){
  
		System.out.println((args.length>2) ? Integer.parseInt(args[0]) % 10:Integer.parseInt(args[1]) / 20);

	}
}

/*
  Explanation : As args[] array is of string array so strings get concatenated 
  		20/20 = 1
