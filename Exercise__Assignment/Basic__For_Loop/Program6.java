/*
 

   WAP to print the sum of all even number and multiplication of odd number between 1 to 10.


*/


class Number {

	public static void main(String[] args) {

		int num = 10;
		int sum = 0;
		int mult = 1;

		for(int i=1;i<=num;i++) {

			if(i % 2 == 0) {

				sum = sum + i;

			}else{

				mult = mult * i;

			}
		}
		System.out.println(sum);
		System.out.println(mult);
	}
}
