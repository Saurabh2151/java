/*
 
   WAP take size of array from user and also take integer element from user find the minimum element from the array.

   */

import java.io.*;

class ArrayDemo {

static	int num;

	public static void main(String[] args)throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Array Size :");

		int size = Integer.parseInt(br.readLine());

		System.out.println("Enter the Element :");

		int arr[] = new int[size];

		for(int i=0;i<arr.length;i++) {

			arr[i] = Integer.parseInt(br.readLine());

		}
		
		int min = arr[0];
		for(int element=0;element<arr.length;element++) {
				
			if(arr[element] < min) {

				min = arr[element];
	
			}
			
		}
		System.out.println("Minimum Element = "+min);
	} 
}



