/* 
 
   WAP to count the digit of the given number.


*/


class Number {

	public static void main(String[] args) {

		int num = 942111423;
		int count = 0;

		while(num != 0) {

			count++;
			num = num / 10;
		}
		System.out.println(count);
	}
}
