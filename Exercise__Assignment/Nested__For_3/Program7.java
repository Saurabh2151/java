/*

  F  
  E  F  
  D  E  F  
  C  D  E  F  
  B  C  D  E  F  
  A  B  C  D  E  F  


*/


class Pattern {

        public static void main(String[] args) {

                int row = 6;
		
	       	for(int i=1;i<=row;i++) {

			char ch = (char)(64+row-i+1);

                        for(int j=1;j<=i;j++) {

                                System.out.print(ch+"  ");
                                ch++;

			}
                        System.out.println();
                }
        }
}

