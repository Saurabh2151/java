
/*
 
   WAP take 7 character as an input print only vowels from the array.

   */


import java.io.*;
class ArrayDemo {

	public static void main(String[] args)throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Array Size :");

		int size = Integer.parseInt(br.readLine());

		System.out.println("Enter the Element :");

		char arr[] = new char[size];

		for(char i=0;i<arr.length;i++)	{

			arr[i] = br.readLine().charAt(0);

		}

		for(char j=0;j<arr.length;j++) {

			char num = arr[j];

			if(num == 'a' || num == 'e'  || num == 'i' || num == 'o' || num == 'u' || 
					num == 'A' || num == 'E' || num == 'I' || num == 'O' || num == 'U') {

				System.out.print(num+" ");

			}
		}
		System.out.println();
	}

}
