

/*
 
   WAP to take size of array from user and also take integer elements from user print Sum All Element.

 */


import java.io.*;

class ArrayDemo {

	public static void main(String[] args)throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
		System.out.println("Enter the Array Size :");

		int size = Integer.parseInt(br.readLine());

		System.out.println("Enter the Element :");

		int arr[] = new int[size];
			
		int sum = 0;

		for(int i=0;i<size;i++) {

			arr[i] = Integer.parseInt(br.readLine());

			sum = sum + arr[i];

		}
		System.out.println("Sum ="+sum);
	}
}


	

