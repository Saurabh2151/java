/*
 

	WAP to print the Fibonacci series.

	Fibonacci Series ==> The sequence type series where each number is the sum of the two precede it.

 		Ex : 0 1 1 2 3 5 8 13 21 34 55.
*/


class Number {

	public static void main(String[] args) {
		
		int N = 10;
		int prev = 1;
		int start = 0;
		int sum = 0;

		for(int i=1;i<=N;i++) {

			System.out.println(sum);
			
			sum = prev + start;
			prev = start;
			start = sum;
		}
	}
}
