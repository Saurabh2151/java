/*
 
	WAP to print composite Number.

   Composite Number ==> The Numbers that have more than two Factors.

 
*/



class Number {

	public static void main(String[] args) {

		int N = 30;
		int count = 0;

		for(int i=1;i<=N;i++) {

			if(N % i == 0) {

				count++;

			}
		}
		if(count > 2) {

			System.out.println(N + " is a composite Number");

		}else if(count == 1) {

			System.out.println(N+ " is nither composite nor prime");

		}else{

			System.out.println(N+ " is not composite Number");

		}
	}
}

