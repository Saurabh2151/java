

// Pythagon Triplet

// a*a + b*b = c*c.


class Triplet {

	public static void main(String[] args) {
		
		int a = 3;
		int b = 4;
		int c = 5;

		if(a*a + b*b == c*c || b*b + c*c == a*a || a*a + c*c == b*b) {

			System.out.println("It is a Pythagon Triplet");

		}else{

			System.out.println("It is not Pythagon Triplet");

		}
	}
}
