/*

  Method : public String Concat(String str);

  Description : - Concatination String to the String i.e Another String is Concatination
  		  with the first String.
		- Implement new array of character whose length is sum of str1.length and str2.length.

  Parameter : String

  return Type : String  

*/


class ConcatDemo {

	public static void main(String[] args) {

		String str1 = "Core2";
		String str2 = "Web";

		String str3 = str1.concat(str2);

		System.out.println(str3);
	}
}
