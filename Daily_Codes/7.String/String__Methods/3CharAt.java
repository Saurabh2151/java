

/*
 

Method : public char chatAt(int index)

Description : - It returns the charactre located at specified index within the given string

Parameter : Integer(Index)

return Type : Character

*/

class CharAtDemo {

	public static void main(String[] args) {

		String str1 = "Core2Web";

		System.out.println(str1.charAt(4));
		System.out.println(str1.charAt(0));
		System.out.println(str1.charAt(8));   // StringIndexOutOfBoundException
	}
}
