

/*
  
  Method : public boolean equals(Object an Object)

  Description : Predicate with compare an object to this.
  		This is true only for string returns true
		 if an object is semantically equal to this.

  Paranmeter : object (an object)

  return Type : boolean

  */

class EqualsDemo {

	public static void main(String[] args) {

		String str1 = "Shash";
		String str2 = new String("Shashi");

		System.out.println(str1.equals(str2));

	}
}
