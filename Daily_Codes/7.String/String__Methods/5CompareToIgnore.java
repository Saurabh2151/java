
/*
 
  Method : Public int CompareToIgnaoreCase(String str)

  Description : It compare str1 and str2 (Case Insensitive)

  Parameter : String 

  return Type : Integer

 */

class CompareToIgnore {

	public static void main(String[] args) {

		String str1 = "Core2Web";

		String str2 = "Core2";

		System.out.println(str1.compareToIgnoreCase(str2));
	}
}
