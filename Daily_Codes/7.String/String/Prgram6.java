

class StringDemo {

	public static void main(String[] args) {

		String str1 = "Shashi";
		String str2 = "Bagal";

		System.out.println(str1);
		System.out.println(str2);

		str1.concat(str2);              // intrnally call by concat().

		System.out.println(str1);   
		System.out.println(str2);

	}
}


/*
  ans : concat kelya nantr str1 la variable madhe Strore nahi kel
	Heap vr fkt object tayar hoto pn tyacha reference nahi
  */
