


class StringDemo {

	public static void main(String[] args) {

		String str1 = "Shashi";

		String str2 = "Bagal";

		String str3 = str1 + str2;     // Internally call append() in Stringbuilder class

							// str3 and str4 are not a same.

		String str4 = str1.concat(str2);   // Internally call concat() 

		System.out.println(str3);
		System.out.println(str4);

	}
}
