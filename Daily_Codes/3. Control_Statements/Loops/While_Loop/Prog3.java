

// Take on Integer N as Input.

// print odd integer 1 to N Using while Loop.



class Integer {

	public static void main(String[] args) {

		int i = 1;
		int N = 10;

		while(i <= N) {

			if(i % 2 == 1) {

				System.out.println(i);
			}
			i++;
		}
	}
}
