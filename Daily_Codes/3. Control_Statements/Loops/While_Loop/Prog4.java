


// Integer N as Input.

// Multiple of 4 till N.


class Multiple {

	public static void main(String[] args) {

		int i = 4;
		int N = 22;

		while(i <= N) {

			System.out.println(i);
			i = i+4;
		}
	}
}
