


// Given an integer N .

// Print Multiple of it's digit.

// Input = 135    Output = 15.



class Number {

	public static void main(String[] args) {

		int N = 135;

		int Mult = 1;

		while(N != 0) {

			int ret = N % 10;

			Mult = Mult * (ret);

			N = N / 10;

		}
		System.out.println(Mult);
	}
}
