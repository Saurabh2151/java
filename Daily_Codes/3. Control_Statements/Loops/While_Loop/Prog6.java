

// Given an integer N.

// Print sum of it's digit.

// Assume = n >= 0.      Input = 6531   output = 15.



class Number {

	public static void main(String[] args) {

		int N = 6531;

		int sum = 0;

		while(N != 0) {

			int ret = N % 10;

			sum = sum + ret;

			N = N / 10;

		}
		System.out.println(sum);
	}
}
