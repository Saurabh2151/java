


// Integer N as Input.
 
// Print Perfect square till N.

// Perfect square : A integer whose square root is a integer.

// input = 30    output = 1,4,9,16,25.


class Number {

	public static void main(String[] args) {

		int N = 30;
		int i = 1;

		while(i * i <= N) {

			System.out.println(i * i);

			i++;

		}
	}
}


